import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProductionComponent } from './production/production.component';

const routes:Routes=[
  {
   path:"dashboard",
   component: ProductionComponent
},

]

@NgModule({
  declarations: [],

  imports: [
      RouterModule.forChild(routes),
    CommonModule
  ],

})
export class ProductionsRoutingModule { }
