import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-conseiller',
  templateUrl: './conseiller.component.html',
  styleUrls: ['./conseiller.component.scss']
})
export class ConseillerComponent implements OnInit {


  constructor(private monhttp:HttpService) { }
  orders:any=[]
  
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  crea_print_items:any=[]
  crea_okprint_items:any=[]
  crea_acpprint_items:any=[]
  crea_pack_items:any=[]
  crea_okpack_items:any=[]
  crea_acppack_items:any=[]
  crea_gadget_items:any=[]
  crea_okgadget_items:any=[]
  crea_acpgadget_items:any=[]
  crea_cloth_items:any=[]
  crea_okcloth_items:any=[]
  crea_acpcloth_items:any=[]
  crea_disp_items:any=[]
  crea_okdisp_items:any=[]
  crea_acpdisp_items:any=[]
  editor_items:any=[]
  okeditor_items:any=[]
  acpeditor_items:any=[]
  ngOnInit(): void {
   
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
        console.log(this.orders)
        for(let item of this.orders){
       
          for(let i =0;i<this.orders.length;i++){
           this.orders[i].description= JSON.parse(this.orders[i].description);
          
          }
          if(item.description.type_product=="crea" && item.description.category=="vetement"){
            this.crea_cloth_items.push(item)
            
           }
           
          if(item.description.type_product=="editor" ){
            this.editor_items.push(item)
          }
         console.log(this.crea_print_items)
          }
},
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        for(let item of this.pendingorders){
          for(let i =0;i<this.pendingorders.length;i++){
            this.pendingorders[i].description= JSON.parse(this.pendingorders[i].description)
           }
  
           if(item.description.type_product=="crea" && item.description.category=="vetement"){
            this.crea_acpcloth_items.push(item)
            
           }
          if(item.description.type_product=="editor" ){
            this.acpeditor_items.push(item)
          }
         console.log(this.crea_print_items)
          }
      },
      err=>{
        console.log(err)
      }

    )
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      console.log(this.okorders)
      for(let items of this.okorders){
        for(let i =0;i<this.okorders.length;i++){
          this.okorders[i].description= JSON.parse(this.okorders[i].description)
  
         
         }
         if(items.description.type_product=="crea" && items.description.category=="vetement"){
          this.crea_okcloth_items.push(items)
          
         }
        
        if(items.description.type_product=="editor" ){
          this.acpeditor_items.push(items)
        }
  
      }
      console.log(this.crea_cloth_items)
      
    }
    ,
      err=>{
        console.log(err)
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
    
     for(let i =0;i<this.okorders.length;i++){
      this.cancelorders[i].description= JSON.parse(this.cancelorders[i].description)
      console.log(this.cancelorders[i].description)
     }
    
    },
    err=>{
      console.log(err)
    }
  )
  }

updateStatus(event:any){
 let id = event.target.id
 if(id!=undefined){
  
   let data={id:id,status:'pending'}
   this.monhttp.Updatestatus(data).subscribe(
     res=>{
       console.log(res)
       if(res.status==true){
         this.monhttp.Getpending().subscribe(
           res=>{
             //console.log(res)
             this.pendingorders=res.data
             console.log( this.pendingorders)
           }
         )
       }
     },
     err=>{
       console.log(err)
     }
   )
 }
 
}
updateok(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'ok'}
    this.monhttp.updateok2(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.GetordersLivre().subscribe(
            res=>{
              //console.log(res)
              this.okorders=res.data
              console.log(this.okorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}
updatecancel(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'rejected'}
    this.monhttp.Updatecancel(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.getCancel().subscribe(
            res=>{
              //console.log(res)
              this.cancelorders=res.data
              console.log(this.cancelorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}



logout(){
  localStorage.removeItem('user');
  localStorage.removeItem('role');
  location.reload();
}
}
